from locust import HttpUser, TaskSet, task, between
from locust.clients import HttpSession


class UserTasks(TaskSet):


    @task
    def index(self):
        self.client.get("/kungfutest/autotest", name="test-basic")


class MultipleHostsUser(HttpUser):
    wait_time = between(5, 15)
    tasks = [UserTasks]
