from flask import Flask

import json
import socket
import os
import requests

app = Flask(__name__)

@app.route('/')
def index():
    return json.dumps({
        'hostname': socket.gethostname()})

@app.route('/kungfutest/<string:id>')
def taxpayer(id):
    if id == "":
        id = "not known"
    # Get the upstream address
    upstream = os.environ.get('UPSTREAM_ADDRESS')
    # Hit the upstream server
    r = requests.get(url = upstream)
    return json.dumps({'id': id, 'response': r.text})

# @app.errorhandler(Exception)
# def page_not_found(e):
#     print (e)
#     return json.dumps({'error': "not found"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
