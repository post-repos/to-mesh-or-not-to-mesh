#!/bin/bash
#
# Monitor overall Kubernetes cluster utilization and capacity.
#
# Original source:
# https://github.com/kubernetes/kubernetes/issues/17512#issuecomment-367212930
#
# Tested with:
#   - AWS EKS v1.11.5
#
# Does not require any other dependencies to be installed in the cluster.

set -e

KUBECTL="kubectl"
NODES=$($KUBECTL get nodes --no-headers -o custom-columns=NAME:.metadata.name)
TIMES=0
MEM=0
CPU=0
PODMEM=0
PODCPU=0

usage() {
  while true;
  do
    local node_count=0
    local total_percent_cpu=0
    local total_percent_mem=0
    local readonly nodes=$@

    for n in $nodes; do
      local requests=$($KUBECTL describe node $n | grep -A3 -E "\\s\sRequests" | tail -n2)
      local percent_cpu=$(echo $requests | awk -F "[()%]" '{print $2}')
      local percent_mem=$(echo $requests | awk -F "[()%]" '{print $8}')

      node_count=$((node_count + 1))
      total_percent_cpu=$((total_percent_cpu + percent_cpu))
      total_percent_mem=$((total_percent_mem + percent_mem))
    done

    local avg_percent_cpu=$((total_percent_cpu / node_count))
    local avg_percent_mem=$((total_percent_mem / node_count))

    echo "Average request: ${avg_percent_cpu}% CPU, ${avg_percent_mem}% memory."

    local pod_count=0
    local pod_total_percent_cpu=0
    local pod_total_percent_mem=0

    while IFS= read -r i;
    do 
        local mem_usage=$(echo $i | awk '{print $2}' | sed 's/[^0-9]*//g')
        local cpu_usage=$(echo $i | awk '{print $3}' | sed 's/[^0-9]*//g')
        pod_total_percent_cpu=$(( pod_total_percent_cpu + cpu_usage ))
        pod_total_percent_mem=$(( pod_total_percent_mem + mem_usage ))
        pod_count=$(( pod_count + 1 ))

    done < <(kubectl top po -n kungfootest --no-headers)

    local avg_pod_percent_cpu=$((pod_total_percent_cpu / pod_count))
    local avg_pod_percent_mem=$((pod_total_percent_mem / pod_count))
    echo "Average usage: ${avg_pod_percent_cpu}% CPU, ${avg_pod_percent_mem}% memory."

    MEM=$(( MEM + avg_percent_mem ))
    CPU=$(( CPU + avg_percent_cpu ))
    PODMEM=$(( PODMEM + avg_pod_percent_mem ))
    PODCPU=$(( PODCPU + avg_pod_percent_cpu ))
    TIMES=$(( TIMES + 1 ))
    sleep 5
  done
}

exit_func() {
    local avg_percent_mem=$(( MEM / TIMES ))
    local avg_percent_cpu=$(( CPU / TIMES ))
    local avg_pod_percent_mem=$(( PODMEM / TIMES ))
    local avg_pod_percent_cpu=$(( PODCPU / TIMES ))
    echo " "
    echo " "
    echo "Times: $TIMES"
    echo "Total average requests: ${avg_percent_cpu}% CPU, ${avg_percent_mem}% memory."
    echo "Total average usage: ${avg_pod_percent_cpu}% CPU, ${avg_pod_percent_mem}% memory."
    exit
}

trap exit_func SIGINT
usage $NODES

